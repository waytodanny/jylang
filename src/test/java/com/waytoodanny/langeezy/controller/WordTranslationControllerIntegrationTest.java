package com.waytoodanny.langeezy.controller;

import com.waytoodanny.langeezy.CommonContextIntegrationTest;
import com.waytoodanny.langeezy.domain.Language;
import com.waytoodanny.langeezy.repository.WordTranslationRepository;
import com.waytoodanny.langeezy.repository.entity.WordTranslationEntity;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.util.Set;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Transactional
@AutoConfigureMockMvc
public class WordTranslationControllerIntegrationTest extends CommonContextIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WordTranslationRepository repository;

    @Before
    public void setUp() {
        var demoTransl = new WordTranslationEntity();
        demoTransl.setWord("boy");
        demoTransl.setLangFrom(Language.ENGLISH.toString());
        demoTransl.setLangTo(Language.RUSSIAN.toString());
        demoTransl.setMeanings(Set.of("мальчик", "парень"));

        repository.save(demoTransl);
    }

    @Test
    public void getExistingTranslation() throws Exception {
        mockMvc
                .perform(get("/word/translation?word=boy&langFrom=ENGLISH&langTo=RUSSIAN"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.word", equalTo("boy")))
                .andExpect(jsonPath("$.languageFrom", equalTo("ENGLISH")))
                .andExpect(jsonPath("$.languageTo", equalTo("RUSSIAN")))
                .andExpect(content().string(containsString("мальчик")));
    }

    @Test
    public void getNotExistingTranslation() throws Exception {
        mockMvc
                .perform(get("/word/translation?word=dog&langFrom=ENGLISH&langTo=RUSSIAN"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createNewAbsentTranslation() throws Exception {
        mockMvc
                .perform(get("/word/translation?word=girl&langFrom=ENGLISH&langTo=RUSSIAN"))
                .andExpect(status().isNotFound());

        var toCreate = "{\"word\":\"girl\",\"languageFrom\":\"ENGLISH\",\"languageTo\":\"RUSSIAN\",\"meanings\":[\"девочка\",\"девушка\"],\"id\":-1}";
        mockMvc
                .perform(post("/word")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(toCreate))
                .andExpect(status().isCreated())
                .andExpect(redirectedUrl("http://localhost/word/translation%3Fword=girl&langFrom=ENGLISH&langTo=RUSSIAN"));

        mockMvc
                .perform(get("/word/translation?word=girl&langFrom=ENGLISH&langTo=RUSSIAN"))
                .andExpect(status().isOk());
    }

    @Test
    public void createAlreadyPresentTranslation() throws Exception {
        var toCreate = "{\"word\":\"boy\",\"languageFrom\":\"ENGLISH\",\"languageTo\":\"RUSSIAN\",\"meanings\":[\"парень\",\"мальчик\"],\"id\":-1}";
        mockMvc
                .perform(post("/word")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(toCreate))
                .andExpect(status().isConflict());
    }
}