package com.waytoodanny.langeezy.controller;

import com.waytoodanny.langeezy.domain.Language;
import com.waytoodanny.langeezy.domain.WordTranslation;
import com.waytoodanny.langeezy.service.WordTranslationService;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
public class WordTranslationController {

    private final WordTranslationService service;

    public WordTranslationController(WordTranslationService service) {
        this.service = service;
    }

    @GetMapping(value = "/word/translation", produces = "application/json")
    public ResponseEntity<WordTranslation> get(@RequestParam("word") String word,
                                               @RequestParam("langFrom") Language langFrom,
                                               @RequestParam("langTo") Language langTo) {
        return service.get(word, langFrom, langTo)
                .map(t -> new ResponseEntity<>(t, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(value = "/word", consumes = "application/json")
    public ResponseEntity<WordTranslation> create(@RequestBody WordTranslation translation,
                                                  UriComponentsBuilder uriBuilder) {

        return service.create(translation)
                .map(created -> Pair.of(created, uriBuilder
                        .path("/word/translation?word={word}&langFrom={lf}&langTo={lt}")
                        .build(created.getWord(), created.getLanguageFrom(), created.getLanguageTo()))
                )
                .map(p -> ResponseEntity.created(p.getSecond()).body(p.getFirst()))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.CONFLICT));
    }
}
