package com.waytoodanny.langeezy.service.impl;

import com.waytoodanny.langeezy.domain.Language;
import com.waytoodanny.langeezy.domain.WordTranslation;
import com.waytoodanny.langeezy.repository.WordTranslationRepository;
import com.waytoodanny.langeezy.repository.entity.WordTranslationEntity;
import com.waytoodanny.langeezy.service.WordTranslationService;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.function.Function;

@Service
public class WordTranslationServiceImpl implements WordTranslationService {

    private final WordTranslationRepository repository;

    public WordTranslationServiceImpl(WordTranslationRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<WordTranslation> get(String word, Language languageFrom, Language languageTo) {
        return find(word, languageFrom.toString(), languageTo.toString())
                .map(toDomainConverter());
    }

    @Override
    public Optional<WordTranslation> create(@NonNull WordTranslation translation) {
        return Optional.of(translation)
                .map(toEntityCreationConverter())
                .filter(e -> find(e.getWord(), e.getLangFrom(), e.getLangTo()).isEmpty())
                .map(repository::save)
                .map(toDomainConverter());
    }

    private Optional<WordTranslationEntity> find(String word, String langFrom, String langTo) {
        return repository.findByWordAndLangFromAndLangTo(word, langFrom, langTo);
    }

    private Function<WordTranslation, WordTranslationEntity> toEntityCreationConverter() {
        return source -> {
            var entity = new WordTranslationEntity();
            entity.setWord(source.getWord());
            entity.setLangFrom(source.getLanguageFrom().toString());
            entity.setLangTo(source.getLanguageTo().toString());
            entity.setMeanings(source.getMeanings());
            return entity;
        };
    }

    private Function<WordTranslationEntity, WordTranslation> toDomainConverter() {
        return entity -> WordTranslation.builder()
                .id(entity.getId())
                .word(entity.getWord())
                .languageFrom(Language.valueOf(entity.getLangFrom()))
                .languageTo(Language.valueOf(entity.getLangTo()))
                .meanings(entity.getMeanings())
                .build();
    }
}
