package com.waytoodanny.langeezy.service;

import com.waytoodanny.langeezy.domain.Language;
import com.waytoodanny.langeezy.domain.WordTranslation;

import java.util.Optional;

public interface WordTranslationService {

    Optional<WordTranslation> get(String word, Language languageFrom, Language languageTo);

    Optional<WordTranslation> create(WordTranslation translation);
}
