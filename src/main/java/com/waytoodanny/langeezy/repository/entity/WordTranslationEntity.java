package com.waytoodanny.langeezy.repository.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.FetchType.EAGER;

@Setter
@Getter
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"word", "language_from", "language_to"}))
@Entity
public class WordTranslationEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "word", nullable = false, updatable = false)
    private String word;

    @Column(name = "language_from", nullable = false, updatable = false)
    private String langFrom;

    @Column(name = "language_to", nullable = false, updatable = false)
    private String langTo;

    @ElementCollection(targetClass = String.class, fetch = EAGER)
    private Set<String> meanings;
}
