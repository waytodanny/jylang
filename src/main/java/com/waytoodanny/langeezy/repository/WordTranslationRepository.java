package com.waytoodanny.langeezy.repository;

import com.waytoodanny.langeezy.repository.entity.WordTranslationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WordTranslationRepository extends CrudRepository<WordTranslationEntity, Long> {
    Optional<WordTranslationEntity> findByWordAndLangFromAndLangTo(String word, String langFrom, String langTo);
}
