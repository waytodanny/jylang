package com.waytoodanny.langeezy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LangeezyApplication {

    public static void main(String[] args) {
        SpringApplication.run(LangeezyApplication.class, args);
    }

}
