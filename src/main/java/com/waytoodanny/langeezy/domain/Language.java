package com.waytoodanny.langeezy.domain;

public enum Language {
    ENGLISH,
    RUSSIAN
}
