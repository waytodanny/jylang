package com.waytoodanny.langeezy.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Set;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WordTranslation {
    private String word;
    private Language languageFrom;
    private Language languageTo;
    private Set<String> meanings;
    private long id;
}
