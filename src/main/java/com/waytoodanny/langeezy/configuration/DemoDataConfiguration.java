package com.waytoodanny.langeezy.configuration;

import com.waytoodanny.langeezy.domain.Language;
import com.waytoodanny.langeezy.repository.WordTranslationRepository;
import com.waytoodanny.langeezy.repository.entity.WordTranslationEntity;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Set;

@Configuration
@Profile("!integration-test")
public class DemoDataConfiguration {

    @Bean
    public CommandLineRunner bean(WordTranslationRepository repository) {
        return args -> {
            var demoTransl = new WordTranslationEntity();
            demoTransl.setWord("boy");
            demoTransl.setLangFrom(Language.ENGLISH.toString());
            demoTransl.setLangTo(Language.RUSSIAN.toString());
            demoTransl.setMeanings(Set.of("мальчик", "парень"));

            repository.save(demoTransl);
        };
    }
}
